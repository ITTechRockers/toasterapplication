package uvtechsoft.shree.toasterlibrary;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by shree on 6/24/2019.
 */

public class ToasterMessage {

    public static void s(Context c, String message){

        Toast.makeText(c,message,Toast.LENGTH_SHORT).show();

    }
}
